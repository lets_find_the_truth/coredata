//
//  TableViewCell.swift
//  CoreDataNK
//
//  Created by Admin on 9/12/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var passLabel: UILabel!
}
