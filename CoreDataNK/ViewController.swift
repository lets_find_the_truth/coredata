//
//  ViewController.swift
//  CoreDataNK
//
//  Created by Admin on 9/10/18.
//  Copyright © 2018 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var ageTextField: UITextField!
    @IBOutlet weak var passTextField: UITextField!
    @IBOutlet weak var usersTableView: UITableView!
    
    private var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let app = UIApplication.shared.delegate as! AppDelegate
        let context = app.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        request.sortDescriptors = [sortDescriptor]
        fetchedResultsController = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        do {
            try fetchedResultsController.performFetch()
        } catch { print("error")}
        fetchedResultsController.delegate = self
//        let app = UIApplication.shared.delegate as! AppDelegate
//        let context = app.persistentContainer.viewContext
        //add new user
//        let description = NSEntityDescription.entity(forEntityName: "User", in: context)
//        let user = NSManagedObject(entity: description!, insertInto: context)
//        user.setValue(i, forKey: "age")
//        user.setValue("Vlados", forKey: "name")
//        user.setValue("pass\(i)", forKey: "password")
//        app.saveContext()
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
//        let ageSortDescriptor = NSSortDescriptor(key: "age", ascending: true)
//        let nameSortDescriptor = NSSortDescriptor(key: "name", ascending: true)
//        request.sortDescriptors = [nameSortDescriptor, ageSortDescriptor]
//        request.predicate = NSPredicate(format: "name LIKE[cd] %@ AND age > %d", "*m", 3) //cd for any register case and diacritic insensitivity, can use CONTAINTS BEGINSWITH ENDSWITH LIKE(like containt but with more control * - 0 or more, ? - one character) MATCHES(regex syntax
        
//        do {
//            let resutls = try context.fetch(request)
//            for result in resutls as! [NSManagedObject] {
//                print(result.value(forKey: "name") as! String)
//            }
//        } catch {}
    }

    @IBAction func addDitTap(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let entityDescription = NSEntityDescription.entity(forEntityName: "User", in: context)
//        let user = NSManagedObject(entity: entityDescription!, insertInto: context)
//        user.setValue(nameTextField.text, forKey: "name")
//        user.setValue(Int(ageTextField.text ?? "0"), forKey: "age")
//        user.setValue(passTextField.text, forKey: "password")
        //OR
//        let user = User(entity: entityDescription!, insertInto: context)
        //OR
        let user = User(context: context)
        user.name = nameTextField.text
        user.age = Int16(ageTextField.text ?? "0")!
        user.password = passTextField.text
        appDelegate.saveContext()

    }


}

extension ViewController: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        usersTableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .delete:
            if let indexPath = indexPath {
                usersTableView.deleteRows(at: [indexPath], with: .automatic)
            }
        case .insert:
            if let indexPath = newIndexPath{
                usersTableView.insertRows(at: [indexPath], with: .automatic)
            }
        case .move:
            if let indexPath = indexPath, let newIndexPath = newIndexPath {
                usersTableView.moveRow(at: indexPath, to: newIndexPath)
            }
        case .update:
            if let indexPath = indexPath, let cell = usersTableView.cellForRow(at: indexPath) {
                usersTableView.reloadRows(at: [indexPath], with: .automatic)
            }
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        usersTableView.endUpdates()
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        if let frc = fetchedResultsController {
            return frc.sections!.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        if let user = fetchedResultsController.object(at: indexPath) as? NSManagedObject {
            cell.ageLabel.text = "\(user.value(forKey: "age") as? Int ?? 0)"
            cell.nameLabel.text = user.value(forKey: "name") as? String
            cell.passLabel.text = user.value(forKey: "password") as? String
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController.sections else  {
            return 0
        }
        let section = sections[section]
        return section.numberOfObjects
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "delete") { (action, indexPath) in
            self.tableView(tableView, commit: .delete, forRowAt: indexPath)
        }
        let editAcition = UITableViewRowAction(style: .normal, title: "edit") { (action, indexPath) in
            self.tableView(tableView, commit: .none, forRowAt: indexPath)
        }
        return [deleteAction]
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let user = fetchedResultsController.object(at: indexPath) as! NSManagedObject
        let app = UIApplication.shared.delegate as! AppDelegate
        let context = app.persistentContainer.viewContext
        switch editingStyle {
        case .delete:
            context.delete(user)
            app.saveContext()
        case .none:
            break
        default:
            break
        }

        
    }
}

